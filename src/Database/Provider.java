package Database;

import java.sql.SQLException;
import java.util.ArrayList;
import org.joda.time.DateTime;
import SQL.DopeDBException;
import SQL.DopeResultSetException;
import SQL.KSDriftConnection;
import SQL.OracleFormats;
import SQL.SqlQuery;
import BuilderPackage.ObjectInterface;
import DataProcessing.Callqueue;

public class Provider implements ProviderInterface{
	private KSDriftConnection conn; 
	public Provider() throws DopeDBException{
		initSql();
	}
	private void initSql() throws DopeDBException {
		try {
			conn = new KSDriftConnection();
		} catch (DopeDBException e) {
			throw e;
		}
	}
		

	@Override
	public ArrayList<ObjectInterface> obtainData(DateTime start, DateTime end,
			String name) throws SQLException {
		System.out.println(start);
		System.out.println(end);
		ArrayList<ObjectInterface> gg = new ArrayList<ObjectInterface>();
		System.out.println("her");
		SqlQuery query = null;
			query = new SqlQuery(conn, 
					"SELECT K�, TRUNC(TID, 4) AS TID, SUM(ANTAL_KALD) AS ANTAL_KALD FROM KS_DRIFT.PERO_NKM_K�_KVART WHERE DATO " +
							"BETWEEN "+OracleFormats.convertDate(start.toDate())+
							"AND "+OracleFormats.convertDate(end.toDate())+
							" AND ANTAL_KALD > 0 GROUP BY K�, TRUNC(TID, 4)"
					);
			try {
				query.execute();
				while (query.next()) {
					double decimalTime = query.getDouble("TID");
					decimalTime = decimalTime * 24;
					int callAmount = query.getInteger("ANTAL_KALD");
					ObjectInterface o = new Callqueue(query.getString("k�"));
					boolean changedCurrent = false;
					for (ObjectInterface objectInterface : gg) {
						if (objectInterface.getType().equalsIgnoreCase(query.getString("K�"))) {
							objectInterface.addData(""+decimalTime, callAmount);
							changedCurrent = true;
						}else {
							o = new Callqueue(query.getString("K�"));
							o.addData(""+decimalTime, callAmount);
							changedCurrent = false;
						}
					}
					if (!changedCurrent) {
						gg.add(o);
					}
					if (gg.size() == 0) {
						ObjectInterface o1 = new Callqueue(query.getString("K�"));
						o1.addData(""+decimalTime, callAmount);
						gg.add(o1);
					}
				
					
				}
			} catch (DopeResultSetException | DopeDBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	
		System.out.println(gg);
		return gg;

}
}