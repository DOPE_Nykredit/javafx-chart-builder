package Database;
import java.sql.SQLException;
import java.util.ArrayList;
import org.joda.time.DateTime;
import BuilderPackage.ObjectInterface;

/**
 * This is one of the key interfaces used to obtain data from the database.
 * @author MRCR
 *
 */
public interface ProviderInterface  {

	public ArrayList<ObjectInterface> obtainData(DateTime start, DateTime end, String name) throws SQLException;
	
}
