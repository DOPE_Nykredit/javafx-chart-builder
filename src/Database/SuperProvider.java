package Database;
/**
 * This abstract class is ment for implemention when a Oracle database is used. 
 * Note that this class currently only contains one method that converts a period integer to a char 
 * that can be used when collecting data from the database.
 * @author MRCR
 *
 */
public abstract class SuperProvider  {
	protected String periodConvertOracle(Integer selectionAsInt){
		
		switch (selectionAsInt) {
		case 1:
			return "DD";
		case 2:
			return "MM";
		case 3: 
			return "HH";
		case 4: 
			return "YY";
		default:
			return null;
		}
		
	}

}
