package BuildInCss;

import javafx.scene.Node;
import javafx.scene.effect.Reflection;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

/**
 * Only one instance should be created of this class!
 * This class allows the user of the package to add some special effects to his application!.
 * 
 * @author MRCR
 *
 */
public class SpecialEffects {

	private static SpecialEffects specialEffects = new SpecialEffects();
	
	private SpecialEffects(){
	}
	public static SpecialEffects getInstance(){
		return specialEffects;
	}
	public Node nykreditLogoWithReflection(){
		 Text t = new Text();
	        t.setX(10.0f);
	        t.setY(50.0f);
	        t.setCache(true);
	        t.setText("Nykredit");
	        t.setFill(Color.BLUE);
	        t.setFont(Font.font("null", FontWeight.BOLD, 30));
	 
	        Reflection r = new Reflection();
	        r.setFraction(0.9);
	        
	        t.setEffect(r);
	        t.setTranslateY(-20);
	        return t;
	}
	
}
