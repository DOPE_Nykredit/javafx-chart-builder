package GUI;

import javax.swing.JFrame;

import BuilderPackage.Director;
import BuilderPackage.JFreeChartBuilder;
import BuilderPackage.PeriodSelection;
import Builders.JfreeLineChartBuilder;
import DataProcessing.Statistics;

public class SwingGui extends JFrame {
	
	public static void main(String[] args) {
		Statistics s = new Statistics();
		PeriodSelection ps = new PeriodSelection(PeriodSelection.DAYS);
		JFrame f = new JFrame();
		f.show();
		Director d = new Director(null, null, s);
		JFreeChartBuilder jcb = new JFreeChartBuilder();
		jcb = new JfreeLineChartBuilder();
		d.buildTypeTwo(jcb, "hej", ps, "");
		f.add(jcb.getChart());
	}

}
