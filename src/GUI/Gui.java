package GUI;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import javax.imageio.ImageIO;
import org.joda.time.DateTime;
import BuilderPackage.ChartBuilder;
import BuilderPackage.Director;
import BuilderPackage.PeriodSelection;
import Builders.BarChartBuilder;
import Builders.LineChartBuilder;
import Builders.PieChartBuilder;
import Builders.StackedBarChartBuilder;
import DataProcessing.Statistics;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Reflection;
import javafx.scene.image.WritableImage;
import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class Gui extends Application {
	//important fields
	private static 	BorderPane bp;
	private static Director d;
	private static PeriodSelection ps;
	private static Tab t2;
	private static ChartBuilder cb2 = new LineChartBuilder();
	private static ChartBuilder pie = new PieChartBuilder();
	private static ChartBuilder stacked = new StackedBarChartBuilder();
	private static ChartBuilder bar = new BarChartBuilder();

	private final ObservableList<String> box_choice = FXCollections.observableArrayList(
			"Dag", "Uge","M�ned","�r"
			);
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Statistics s = new Statistics();
		d = new Director(null, null, s);
		launch(args);
	}
	public void saveAsPng(Tab n) {  
		WritableImage image = n.getContent().snapshot(new SnapshotParameters(), null); 
		ClipboardContent cc = new ClipboardContent();
		cc.putImage(image);
		Clipboard.getSystemClipboard().setContent(cc);
		// TODO: probably use a file chooser here 
		System.out.println("File saved");
		File file = new File("testing.png");   
		try {  ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);  } 
		catch (IOException e) { 
			// TODO: handle exception here  } }  
		}
	}


	@Override
	public void start(Stage primaryStage) throws Exception {
		Group root = new Group();
		
		DateTime dt = new DateTime();
		DateTime subtracted = dt.minusDays(6);
		DateTime end = new DateTime();
		bp = new BorderPane();
		d.setStartAndEnd(subtracted, end);
		bp.setTop(createTop());
		bp.setCenter(createCenter());
	
		root.getChildren().add(bp);
		Scene s = new Scene(root);
		root.getChildren().add(reflection());
		s.getStylesheets().add("test.css");

		primaryStage.setScene(s);
		primaryStage.show();
		
	}
	private Node reflection() {
		 Text t = new Text();
	        t.setX(10.0f);
	        t.setY(50.0f);
	        t.setCache(true);
	        t.setText("Nykredit");
	        t.setFill(Color.BLUE);
	        t.setFont(Font.font("null", FontWeight.BOLD, 30));
	 
	        Reflection r = new Reflection();
	        r.setFraction(0.9);
	        
	        t.setEffect(r);
	        t.setTranslateY(-20);
	        return t;

	}
	public Node createTop(){
		DropShadow ds = new DropShadow();
		ds.setOffsetY(3.0);
        ds.setOffsetX(3.0);
        ds.setColor(Color.GRAY);
        
		AnchorPane top = new AnchorPane();
		top.setPrefSize(680, 86);
		ComboBox<String> timeChoice = new ComboBox<>();
		timeChoice.setItems(box_choice);
		timeChoice.setPromptText("V�lg tidsformat");
		timeChoice.setPrefSize(125, 19);
		timeChoice.setLayoutX(155);
		timeChoice.setLayoutY(25);
		timeChoice.setEffect(ds);
		Tooltip timeToolTip = new Tooltip();
		timeToolTip.setText("V�lg den periode \nDu �nsker at se data fra");
		timeChoice.setTooltip(timeToolTip);
		
		Button hent_data = new Button();
		hent_data.setLayoutX(552);
		hent_data.setLayoutY(25);
		hent_data.setText("Hent Data");
		hent_data.setPrefSize(103, 37);


		hent_data.setEffect(ds);
		hent_data.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				cb2.setLegendText("HELLO WORLD!");
				PeriodSelection ps = new PeriodSelection(PeriodSelection.DAYS);
				d.addAdditonalSeries(cb2,ps, "");
				d.addAdditonalSeries(bar, ps, "");
				
				/*try {
					bp.setCenter(createCenter());
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				*/
				
			}
		});
		
		top.getChildren().addAll(timeChoice,hent_data);
		ps = new PeriodSelection(PeriodSelection.YEAR);
		for (String x : ps.getTimeIntervals()) {
			System.out.println(x);
		}

		return top;
		
	}
	public Node createCenter() throws SQLException{
		PeriodSelection ps = new PeriodSelection(PeriodSelection.DAYS);
		AnchorPane ap = new AnchorPane();
		GridPane gp = new GridPane();
		
		ChartBuilder cb = new LineChartBuilder();
		d.buildTypeOne(cb2, "Hello", ps,"");
		TabPane tp = new TabPane();
		TabPane tp2 = new TabPane();
		Tab tabTappane2 = new Tab("tappane2");
		tp2.getTabs().add(tabTappane2);
		
		final Tab t1 = new Tab();
		t1.setText("LineChart");
		t1.setContent(cb2.getChart());	
		t1.getContent().addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				saveAsPng(t1);
				
			}
		});
	
		t2 = new Tab();
		t2.setText("BarChart");
		Tooltip barToolTip = new Tooltip();
		barToolTip.setText("Se bar chart her!");
		t2.setTooltip(barToolTip);
		
		cb.addMouseListner(true);
		String[] testing = {"Hello", "I", "Am", "Testing"};
		ps.setCostumTimeIntervals(testing);
		d.buildTypeOne(bar, "Bar", ps,"");
		t2.setContent(bar.getChart());
		Tab t4 = new Tab("My pie");
		t4.setContent(createT4Content());
		
		Tab t3 = new Tab();
		t3.setText("StackedBarChat");
		d.buildTypeOne(stacked, "hello", ps, "");
		t3.setContent(stacked.getChart());
		
		cb = new PieChartBuilder();
	
	

		for (Tab t : tp.getTabs()) {
			final Tab n = t;
		n.getContent().setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				saveAsPng(n);
				
			}
		});
		}
		Tab tab5 = new Tab();
		ChartBuilder cb5 = new LineChartBuilder();
		d.buildTypeOne(cb5, "Henvendelser", ps, "");
		tab5.setContent(cb5.getChart());
		gp.addRow(0, ap);
		tp.getTabs().addAll(t1,t2,t3,t4, tab5);
		ap.getChildren().addAll(tp,tp2);
		return gp;
	}
	private Node createT4Content() throws SQLException {
		AnchorPane ap = new AnchorPane();
		
		d.buildTypeOne(pie, "PIE", ps,"");
		ap.getChildren().add(pie.getChart());
		return ap;
	}

}
