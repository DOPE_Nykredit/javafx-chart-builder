package SQL;
public class DopeResultSetException extends DopeException {

	private static final long serialVersionUID = 1L;

	public DopeResultSetException(String exceptionMsg, String dopeErrorMsg){
		super(null, exceptionMsg, dopeErrorMsg);
	}
	public DopeResultSetException(Exception e, String exceptionMsg,	String dopeErrorMsg){
		super(e, exceptionMsg, dopeErrorMsg);
	}
}
