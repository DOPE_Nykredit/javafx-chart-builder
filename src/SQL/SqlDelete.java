package SQL;
public class SqlDelete extends SqlStatement {

	private int rowsAffected;
	private boolean executed;
	
	public int getRowsAffected(){
		return this.rowsAffected;
	}
	public boolean getExecuted(){
		return this.executed;
	}
	
	public SqlDelete(DopeDBConnection conn, String sql){
		super(conn, sql);
	}

	@Override
	public synchronized void execute() throws DopeDBException {
		this.rowsAffected = conn.executeDeleteStatement(super.sql);
	}	
}
