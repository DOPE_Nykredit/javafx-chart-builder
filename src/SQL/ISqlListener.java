package SQL;
public interface ISqlListener {

	public void executed();
	public void executionFailed(DopeDBException e);
	
}
