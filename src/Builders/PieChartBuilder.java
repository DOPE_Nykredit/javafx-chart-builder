package Builders;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;

import javafx.scene.control.TableView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import BuilderPackage.ChartBuilder;
import BuilderPackage.ObjectInterface;

/**
 * 
 * @author MRCR
 *
 */
public class PieChartBuilder extends ChartBuilder{
	private TableView<PieChart.Data> tableView = new TableView<>();
	private PieChart pieChart;
	private VBox box = new VBox();
	private ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();
	private Group group = new Group();
	final ObservableList<Node> children = group.getChildren();

	private Label caption;
	@Override
	public void initiate(String title){
		pieChart = new PieChart();
		pieChart.setTitle(title);
		pieChart.setId("PieChart");

	}
	public void saveAsPng() {  
		WritableImage image = pieChart.snapshot(new SnapshotParameters(), null); 
		// TODO: probably use a file chooser here 
		File file = new File("chart.png");   try {  ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);  } 
		catch (IOException e) { 
			// TODO: handle exception here  } }  
		}
	}


	@Override
	public void createTable(){
	}


	@Override
	public void createSeries(){
		/*
		 * dette svare til statistics.CalculateAverage();
		 */
		for (int j = 0; j < objects.size(); j++) {
			for (String x: selection.getTimeIntervals()) {
				pieChartData.add(new PieChart.Data(x, objects.get(j).getData(x)));		

			}

		}
		pieChart.getData().addAll(pieChartData);
		children.addAll(pieChart);
		createMouseEvent();

	}
	private void CreateCopyMouseEvent() {
		box.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				System.out.println(event.getButton());

			}
		});

	}
	private void createMouseEvent() {
		caption = new Label("");
		caption.setTextFill(Color.DARKORANGE);
		caption.setStyle("-fx-font: 24 arial;");
		children.add(caption);

		for (final PieChart.Data data : pieChart.getData()) {

			Tooltip tt = new Tooltip();
			tt.setText(String.valueOf(data.getPieValue()) + "%");

			data.getNode().addEventHandler(MouseEvent.MOUSE_PRESSED,
					new EventHandler<MouseEvent>() {

				@Override public void handle(MouseEvent e) {
					if (e.getButton() == MouseButton.PRIMARY) {

						caption.setTranslateX(e.getSceneX());
						caption.setTranslateY(e.getSceneY());
						caption.setText(String.valueOf(data.getPieValue()) + "%");
						caption.setVisible(true);


					}else {
						Button kopier = new Button("Kopier?");
						kopier.setVisible(true);
						kopier.setLayoutX(e.getSceneX());
						kopier.setLayoutY(e.getSceneY());
						children.add(kopier);
						kopier.setOnAction(new EventHandler<ActionEvent>() {

							@Override
							public void handle(ActionEvent event) {
								saveAsPng();
							}
						});

					}

				}

			});
		}


	}
	@Override
	public void createSymbol(){
		pieChart.setLabelLineLength(10);

	}
	public Node getChart(){


		return group;
	}

	public void update(){

	}
	public void addNewSeriesToChartAndTable(ArrayList<ObjectInterface> data){
		createIndividualSeries(data);
	}	

	public void createIndividualSeries(ArrayList<ObjectInterface> data){
		/*
		 * dette svare til statistics.CalculateAverage();
		 */
		for (int j = 0; j < data.size(); j++) {
			for (String x: selection.getTimeIntervals()) {
				pieChart.getData().addAll(new PieChart.Data(x, data.get(j).getData(x)));		
			}
		}
		createMouseEvent();
	}

}
