package Builders;
import java.util.ArrayList;
import BuilderPackage.ChartBuilder;
import BuilderPackage.ObjectInterface;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;

import javafx.scene.chart.LineChart;

import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;


/**
 * The main class for building a LineChart
 * @author MRCR
 *
 */
public class LineChartBuilder extends ChartBuilder {

	private LineChart<String, Number> line;
	private TableColumn legendCol;    

	public LineChartBuilder(){
		line = new LineChart<>(xAxis, yAxis);
	}
	public void initiate(String title){

		xAxis.setTickLength(0);
		line.setTitle(title);
		line.setLegendVisible(false);
		line.setId("Chart");
	}
	@SuppressWarnings("unchecked")
	public void createTable(){
		tableView = new TableView();
		if (!(line.getData().isEmpty())) {
			legendCol = new TableColumn("Legend");            
			legendCol.setResizable(false);
			legendCol.setCellValueFactory(
					new Callback<CellDataFeatures<XYChart.Series<String,Number>, String>, ObservableValue<XYChart.Series<String,Number>>>() {
						@Override public ObservableValue<Series<String, Number>> call(CellDataFeatures<Series<String, Number>, String> param) {
							return new SimpleObjectProperty(param.getValue());
						}
					}
					);

			legendCol.setCellFactory(new Callback<TableColumn<XYChart.Series<String,Number>,Series<String, Number>>,TableCell<XYChart.Series<String,Number>,Series<String, Number>>>() {
				@Override public TableCell<Series<String, Number>, Series<String, Number>> call(TableColumn<Series<String, Number>, Series<String, Number>> param) {
					return new TableCell<Series<String, Number>, Series<String, Number>>() {
						@Override protected void updateItem(Series<String, Number> series, boolean empty) {
							super.updateItem(series, empty);
							if (series != null) {
								setText(series.getName());
								setGraphic(createSymbol(series, line.getData().indexOf(series)));
							}
						}


					};
				}
			});
			legendCol.setMinWidth(163);
			legendCol.setId("legend");

			legendCol.setText("K� navn og farve");;
			tableView.getColumns().add(legendCol);
			final ObservableList<Data<String, Number>> firstSeriesData = line.getData().get(0).getData();
			for (final Data<String, Number> item: firstSeriesData) {
				TableColumn col = new TableColumn(item.getXValue());
				tableView.setId("Cols");
				col.setSortable(false);
				col.setResizable(false);
				col.prefWidthProperty().bind(line.getXAxis().widthProperty().divide(firstSeriesData.size()));
				col.setCellValueFactory(
						new Callback<CellDataFeatures<XYChart.Series<String,Number>, String>, ObservableValue<Number>>() {
							@Override public ObservableValue<Number> call(CellDataFeatures<XYChart.Series<String,Number>, String> param) {
								/*
								 *for each loop over alle data i listen 
								 */
								for (Data<String, Number> curItem: param.getValue().getData()) {
									if (curItem.getXValue().equals(item.getXValue())) {
										return curItem.YValueProperty();
									}
								}

								return null;
							}
						}
						);
				tableView.getColumns().add(col);
			}

			ObservableList<XYChart.Series<String, Number>> data2 = (line.getData());

			for (XYChart.Series<String,Number> series: data2) {

				tableView.getItems().add(series);
			}

			tableView.setEditable(false);
			tableView.setFocusTraversable(false);
		}
		/*
		 * s�tter tabel h�jden
		 */
		tableView.setTranslateY(-26);
		tableView.setPrefHeight(150);
		tableView.setStyle("-fx-box-border: transparent; -fx-focus-color: transparent; -fx-padding: 0 9 0 9;");
	}
	@Override
	public void setLegendText(String text){
		legendCol.setText(text);
	}
	public void createSeries(){

		series = new XYChart.Series();
		series.setName("Test");
		data = series.getData();
		int testInteger = 0;
		for (int i = 0; i < objects.size(); i++) {
			Series s = new XYChart.Series();
			s.setName(objects.get(i).getType());
			data=s.getData();
			for (String x : selection.getTimeIntervals()) {
				data.add(new XYChart.Data(x,objects.get(i).getData(x)));
			}
			line.getData().add(s);
		}


	}
	private Node createSymbol(Series<String, Number> series, int seriesIndex) {
		Node symbol = new StackPane();
		symbol.getStyleClass().setAll(
				"chart-line-symbol",
				"series" + seriesIndex,
				"default-color" + (seriesIndex % 8)
				);
		return symbol;
	}

	public Node getChart(){
		VBox vbox = new VBox();
		vbox.getChildren().addAll(line, tableView);
		vbox.setAlignment(Pos.TOP_LEFT);
		vbox.setMinWidth(500);
		VBox.setMargin(line, new Insets(0,0,0,132));
		HBox layout = new HBox();
		HBox.setHgrow(vbox, Priority.ALWAYS);
		layout.getChildren().add(vbox);

		return layout;
	}

	public void update(){

	} 
	/**
	 * String Padding. This method allows you to set the padding as you like using the following String:
	 * 0 9 0 8. Note that is the numbers in the String that can be changed
	 * @param padding
	 */
	public void changePadding(String padding){
		String newPadding = "-fx-padding: "+padding+";";
		tableView.setStyle("-fx-box-border: transparent; -fx-focus-color: transparent;"+newPadding);
	}

	public void addNewSeriesToChartAndTable(ArrayList<ObjectInterface> data){
		createIndividualSeries(data);
	}	
	public void createIndividualSeries(ArrayList<ObjectInterface> seriesData){
		series = new XYChart.Series();
		data = series.getData();
		for (int i = 0; i < seriesData.size(); i++) {
			Series s = new XYChart.Series();
			s.setName(seriesData.get(i).getType());
			data=s.getData();
			System.out.println(i);

			for (String x : selection.getTimeIntervals()) {
				data.add(new XYChart.Data(x,seriesData.get(i).getData(x)));
			}
			line.getData().add(s);
			tableView.getItems().add(s);
		}
	}
}
