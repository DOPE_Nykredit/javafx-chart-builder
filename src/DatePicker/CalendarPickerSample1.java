package DatePicker;

/**
 * Copyright (c) 2012 JFXtras Project.  All rights reserved.
 */
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.Line;
import jfxtras.labs.scene.control.CalendarPicker;

/**
 * A date picker, but since Date is deprecated, it picks a Calendar.
 *
 * @see jfxtras.labs.scene.control.CalendarPicker
 */
public class CalendarPickerSample1 extends Application {

    private void init(Stage primaryStage) {
        Group root = new Group();
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root, 300,300));

        GridPane lGridPane = new GridPane();
        root.getChildren().add(lGridPane);
        int lRowIdx = 0;

        {
	        final CalendarPicker lCalendarPicker = new CalendarPicker();
	        lGridPane.add(lCalendarPicker, 0, lRowIdx, 2, 1);
	        lRowIdx++;
	
	        ComboBox<String> lComboBox = new ComboBox<String>(FXCollections.observableArrayList("Single", "Range", "Multiple"));
	        lComboBox.valueProperty().addListener(new ChangeListener<String>()
                {
                    @Override
                    public void changed(ObservableValue<? extends String> observableValue, String oldValue, String newValue)
                    {
                            if (newValue.startsWith("S")) lCalendarPicker.setMode(CalendarPicker.Mode.SINGLE);
                            if (newValue.startsWith("R")) lCalendarPicker.setMode(CalendarPicker.Mode.RANGE);
                            if (newValue.startsWith("M")) lCalendarPicker.setMode(CalendarPicker.Mode.MULTIPLE);
                    }
                });
	        lComboBox.setValue("Single");
	        lComboBox.setPrefWidth(200); 
	        lGridPane.add(new Label("Mode:"), 0, lRowIdx);
	        lGridPane.add(lComboBox, 1, lRowIdx);
	        lRowIdx++;
        }
    }

    public double getSampleWidth() { return 300; }

    public double getSampleHeight() { return 300; }

    @Override public void start(Stage primaryStage) throws Exception {
        init(primaryStage);
        primaryStage.show();
    }
    public static void main(String[] args) { launch(args); }
}
