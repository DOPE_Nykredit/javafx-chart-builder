package DataProcessing;

import java.sql.SQLException;
import java.util.ArrayList;

import org.joda.time.DateTime;

import BuilderPackage.ObjectInterface;
import BuilderPackage.PeriodSelection;
import SQL.DopeDBException;

public interface ProcessInterface {
/**
	 * 
	 * @param name
	 * @param start
	 * @param end
	 * @return
	 * @throws SQLException
	 * @throws DopeDBException
	 */
	public ArrayList<ObjectInterface> getData(String name, DateTime start, DateTime end,PeriodSelection ps) throws SQLException, DopeDBException;
	/**
	 * 
	 * @param contact2
	 */
	void processSingleQueueData(ObjectInterface contact2);
	/**
	 * 
	 * @param queueName
	 * @throws SQLException
	 */
	void obtainNewData(String queueName) throws SQLException;
	
	/**
	 * 
	 * @param name
	 * @param start
	 * @param end
	 * @return
	 */
	boolean doIhaveIt(String name, DateTime start, DateTime end);
	public ArrayList<ObjectInterface> getAdditionalSeriesData(String name, DateTime start, DateTime end, PeriodSelection ps);
	/**
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
}
