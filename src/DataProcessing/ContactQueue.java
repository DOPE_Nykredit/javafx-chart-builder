
package DataProcessing;
import java.util.HashMap;

import javafx.collections.ObservableList;

import BuilderPackage.ObjectInterface;


public abstract class ContactQueue implements ObjectInterface{
	
	private HashMap<String, Integer> data = new HashMap<String, Integer>();
	private String type;
	public ContactQueue(String type){
		this.type = type;
	}
	public String getType(){
		return type;
	}
	public void addData(String key, Integer value){
		if (data.containsKey(key)) {
			Integer i = data.get(key);
			data.put(key, value+i);
		}else {
			data.put(key, value);
		}
	}
	public Integer getData(Object key){
		return data.get(key);
	}
	public String toString(){
		return type;
	}
}


