package DataProcessing;
import java.util.ArrayList;
import org.joda.time.DateTime;
import BuilderPackage.ObjectInterface;
import Database.ProviderInterface;


/**
 * This abstract class main objective is for the client to extend and create his /hers own implementation of the methods
 * However when extending this pattern you should not create a constructor, instead use the super constructor!
 * @author MRCR
 *
 */
public abstract class StatisticPattern {

	
	protected ArrayList<ObjectInterface> cq = new ArrayList<>();
	protected ObjectInterface contact;
	protected ProviderInterface p;
	/**
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	protected boolean checkDatas(DateTime start, DateTime end) {
		return start.toDateMidnight().isEqual(end.toDateMidnight());
	}
	/**
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	protected Integer daysBetween(DateTime start, DateTime end) {
		return  end.dayOfYear().get()-start.dayOfYear().get();

	}
}

