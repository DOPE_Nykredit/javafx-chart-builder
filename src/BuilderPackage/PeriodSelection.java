package BuilderPackage;

import org.joda.time.DateTime;
/**
 * This Class is used as a decorator to the Chartbuilder.
 * it allows the user to specify the Y-Axis of the chart 
 * 
 * Note - To ensure that data is displayed correctly the Period selection should reflect the days between the data you wish to collect.
 * for example if you wish to obtain data from a period of 7 days your periodSelector should be in the state of Days
 * 
 * The class has the following six states:
 * HOUR - Nykredit's opening hours from 8 - 20.00
 * Days - Monday - Sunday
 * Month Jan - Dec
 * Year - Shows the last 4 years
 * Costum - Allows the user to specify a list by adding it manually
 * @author MRCR
 *
 */
public class PeriodSelection{

	public static final int HOUR  = 1;
	public static final int DAYS = 2;
	public static final int MONTH = 3;
	public static final int YEAR = 4;
	private int selectedDate;

	private String[] timeIntervals;

	public PeriodSelection(int selection){
		switch (selection) {
		case 1:
			timeIntervals = new String[]{"8:00", "9:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00"};
			break;
		case 2: 
			timeIntervals = new String[]{"Mandag", "Tirsdag", "Onsdag","Torsdag","Fredag","L�rdag","S�ndag"};
			break;
		case 3: 
			timeIntervals = new String[]{"Jan", "Feb", "Marts", "April", "Maj", "Juni", "Juli", "Agu", "Sep", "Oct", "Nov", "Dec"};
			break;
		case 4:
			DateTime dt = new DateTime();
			timeIntervals = new String[]{""+(dt.getYear()), ""+(dt.getYear()-1), ""+(dt.getYear()-2), ""+(dt.getYear()-3)};
			break;
		default:
			break;
		}
		selectedDate = selection;
	}
	public String[] getTimeIntervals(){
		return timeIntervals;
	}
	public Integer getPeriodAsInt(){
		return selectedDate;

	}
	/**
	 * sets the timeInterval to a costum list.
	 * @param timeIntervals
	 */
	public void setCostumTimeIntervals(String[] timeIntervals){
		this.timeIntervals = timeIntervals;
	}
}