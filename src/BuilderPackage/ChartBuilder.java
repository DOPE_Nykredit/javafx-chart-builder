package BuilderPackage;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.TableView;
import javafx.scene.layout.VBox;

/**
 * The Super class for all the Chartbuilders.
 * @author MRCR
 *
 */
public class ChartBuilder {

    protected final CategoryAxis xAxis = new CategoryAxis();
    protected final NumberAxis yAxis = new NumberAxis();
    protected TableView tableView = new TableView();
    protected ObservableList data;
    protected ArrayList<ObjectInterface> objects;
    protected Series series;
    protected Node symbol;
    protected PeriodSelection selection = new PeriodSelection(PeriodSelection.DAYS);

    public void initiate(String title){

    }
    public void createTable(){

    }
    public  void createSeries(){

    }
    public void createSymbol(){

    }
    public Node getChart(){
        VBox box = new VBox();

        return box;
    }
    public void update(){

    }
    public void addNewSeriesToChartAndTable(ArrayList<ObjectInterface> arrayList){

    }
    /**
     * String Padding. This method allows you to set the Padding as you like using the following String:
     * 0 9 0 8. Note that is the numbers in the String that can be changed
     * @param padding
     */
    public void changePadding(String padding){
    }
    protected void setObjectList(ArrayList<ObjectInterface> data) {
		this.objects = data;
	}
	public void addMouseListner(boolean b) {
		// TODO Auto-generated method stub		
	}
	public void setLegendText(String text){
		
	}
} 

