package BuilderPackage;
import java.lang.reflect.Constructor;
import java.sql.SQLException;
import java.util.ArrayList;
import org.joda.time.DateTime;
import SQL.DopeDBException;
import DataProcessing.ProcessInterface;


public class Director {
	/*
	 * Time moved to information (State pattern)
	 */
	private DateTime start;
	private DateTime end;
	private ProcessInterface stat;
	/**
	 * This is one of two constructors for the Director, do note that it is recommended that you use the 
	 * mediator design pattern to create this director simply because it requires you to have an instance
	 * of the ProcessInterface. Also do note that that the ProcessInterface can come in many forms
	 * Normally it is used as the class statistics.
	 * @param start
	 * @param end
	 * @param stat
	 * {@link Constructor}
	 */
	public Director(DateTime start, DateTime end, ProcessInterface stat){
		this.stat = stat;
		this.start = start;
		this.end = end;
	}
	public Director(ProcessInterface stat){
		this.stat = stat;
	}
	
	/**
	 * 
	 * @param builder
	 * @param title
	 * @param selection
	 * @throws SQLException 
	 */
	public void buildTypeOne(ChartBuilder builder, String title, PeriodSelection selection, String queueName) throws SQLException{
	System.out.println(selection.getPeriodAsInt());
		try {
			builder.setObjectList(stat.getData(queueName, start, end, selection));
		} catch (DopeDBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		builder.selection = selection;
		builder.initiate(title);
		builder.createSeries();
		builder.createSymbol();
		builder.createTable();
	}
	public void addAdditonalSeries(ChartBuilder builder, PeriodSelection selection, String queueName){
		builder.selection = selection;

		builder.addNewSeriesToChartAndTable(stat.getAdditionalSeriesData(queueName, start, end, selection));
	}

	public void buildTypeTwo(JFreeChartBuilder builder, String title, PeriodSelection selection, String queueName){
		try {
			builder.setObjectList(stat.getData(queueName, start, end, selection));
			builder.initiate(title);
		} catch (DopeDBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setStartAndEnd(DateTime s, DateTime e){
		this.start = s;
		this.end = e;
		System.out.println(start + " "+ end);
	}

	// Til andre typer af Data
	public void setCostumTimeFrame(ArrayList<String> time){
	}

}