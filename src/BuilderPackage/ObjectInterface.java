package BuilderPackage;
/**
 * The main interface used for the object processed by the Director & Chartbuilder
 * 
 * Note - The addData and getData relays on a Hashmap that the user of this interface need to create manually
 * @author MRCR
 *
 */
public interface ObjectInterface {

	public String getType();
	public void addData(String key, Integer value);
	public Integer getData(Object key);
}
