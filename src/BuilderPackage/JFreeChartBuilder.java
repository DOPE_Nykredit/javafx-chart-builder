package BuilderPackage;

import java.awt.Dimension;
import java.util.ArrayList;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;

public class JFreeChartBuilder {
	protected JFreeChart chart;
	protected ChartPanel chartPanel;
	protected DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    protected ArrayList<ObjectInterface> objects;
    protected PeriodSelection selection = new PeriodSelection(PeriodSelection.DAYS);
    protected Dimension dimensionChart;
    protected Dimension dimensionTable;
	
    public void initiate(String title){
    	
    }
	public void setObjectList(ArrayList<ObjectInterface> data) {
		this.objects = data;
		
	}
	
	private void createSeries(){
		
	}
	public ChartPanel getChart(){
		return chartPanel;
	}

}
